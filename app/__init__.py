from flask import Flask
import toml
from flask_assets import Environment, Bundle
from flask_login import LoginManager
import os

os.environ['AUTHLIB_INSECURE_TRANSPORT'] = 'true'

app = Flask(__name__, instance_relative_config=True)

login_manager = LoginManager()

app.config.from_file("config.toml", load=toml.load)
app.secret_key = app.config['SECRET_TOKEN']
print(app.config)

login_manager.init_app(app)
login_manager.login_view = "login"

app.jinja_env.lstrip_blocks = True
app.jinja_env.trim_blocks = True

app.logger.setLevel(app.config['LOG_LEVEL'])
app.logger.info(f"Log level is {app.config['LOG_LEVEL']}")

assets = Environment(app)
assets.url = app.static_url_path
assets.directory = app.static_folder
assets.append_path(app.static_folder + '/scss')
scss = Bundle('style.scss', filters='libsass',
              output='all.css', depends="*.scss")
assets.register('scss_all', scss)

from app import models, cli, auth, oidc

login_manager.anonymous_user = models.AnonymousUser
oidc.init(app)

from app.views import dashboard, user, group, client, oidc
