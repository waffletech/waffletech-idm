from flask import request, redirect, url_for, flash
from app import app, models
from urllib.parse import urlparse, urljoin
from functools import wraps
from flask_login import current_user
import datetime


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    is_safe = test_url.scheme in ('http', 'https') and ref_url.netloc == test_url.netloc
    if not is_safe:
        app.logger.warning(f"Rejecting unsafe redirect URL {target}")
    return is_safe


def admin_required(f):
    @wraps(f)
    def decorated_view(*args, **kwargs):
        if not current_user.is_admin:
            flash("Administrator permission is required for that action.")
            return redirect(url_for('dashboard'))
        else:
            return f(*args, **kwargs)
    return decorated_view


def audit_log_self(type, message):
    r = models.AuditEvent()
    r.timestamp = datetime.datetime.now()
    r.actor = current_user
    r.subject = current_user
    r.type = type
    r.message = message
    r.save()

def audit_log_other(subject, type, message):
    r = models.AuditEvent()
    r.timestamp = datetime.datetime.now()
    r.actor = current_user
    r.subject = subject
    r.type = type
    r.message = message
    r.save()
