from app import app, models

import click
import time
import random
import datetime
from flask import Flask


@app.cli.command("init-db")
def init_db():
    models.database.create_tables([models.User, models.Group, models.IncludeGroup, models.Capability,
                                   models.IdSequence, models.AuditEvent,
                                   models.OAuth2Client,
                                   models.OAuth2Token,
                                   models.OAuth2Code])
    models.IdSequence.create(type='uid', counter=14000)
    models.IdSequence.create(type='gid', counter=14000)

    print("Creating groups...")
    user_group = models.Group()
    user_group.name = "users"
    user_group.gid = models.IdSequence.next_id('gid')
    user_group.save()

    svc_group = models.Group()
    svc_group.name = "svc"
    svc_group.gid = models.IdSequence.next_id('gid')
    svc_group.save()

    print("Creating bind user...")
    fname = "Bind"
    lname = "DN"
    username = "bind"
    email = ""
    password = app.config['BIND_PASSWORD']
    bind_user = models.User.make_user(fname, lname, username, email, password)
    bind_user.system_user = True
    bind_user.primary_group = svc_group
    bind_user.save()

    bind_cap = models.Capability()
    bind_cap.userid = bind_user
    bind_cap.action = "search"
    bind_cap.object = "*"
    bind_cap.save()


    print("Creating initial user...")
    admin_group = models.Group()
    admin_group.name = "admin"
    admin_group.gid = models.IdSequence.next_id('gid')
    admin_group.save()

    fname = input('First Name: ')
    lname = input('Last Name: ')
    username = input('Username: ')
    email = input('Email: ')
    password = input('Password: ')
    first_user = models.User.make_user(fname, lname, username, email, password)
    first_user.add_group(admin_group)
    models.AuditEvent.create(timestamp=datetime.datetime.now(), actor=first_user, subject=first_user,
                             type="password-change", message="Initial password set for first user")

    print(f"Database initialized at {app.config['DATABASE']}")
