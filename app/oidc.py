from datetime import datetime, timedelta

from authlib.oidc.core import grants, UserInfo
from authlib.oauth2 import rfc6749, rfc7009, rfc7636, rfc6750
from authlib.integrations.flask_oauth2 import ResourceProtector
from authlib.integrations.flask_oauth2.authorization_server import \
    AuthorizationServer

from app import app
from app.models import User, OAuth2Code, OAuth2Client, OAuth2Token


# OAUTH2 AUTH-CODE GRANT
class AuthCodeMixin:
    def save_authorization_code(self, code, request):
        try:
            client = OAuth2Client.get(id=request.client.id)
        except OAuth2Client.DoesNotExist:
            return None

        nonce = request.data.get('nonce')
        code_challenge = request.data.get('code_challenge')
        code_challenge_method = request.data.get('code_challenge_method')

        return OAuth2Code.create(
            code=code,
            user=request.user,
            client=client,
            redirect_uri=request.redirect_uri,
            response_type=request.response_type,
            scope=request.scope,
            nonce=nonce,
            issued_at=datetime.now(),
            expires_in=app.config['OAUTH2_TOKEN_EXPIRES_IN']['authorization_code'],
            code_challenge=code_challenge,
            code_challenge_method=code_challenge_method)


class AuthCodeGrant(AuthCodeMixin, rfc6749.grants.AuthorizationCodeGrant):
    def query_authorization_code(self, code, client):
        try:
            _client = OAuth2Client.get(id=client.id)
            item = OAuth2Code.get(code=code, client=_client)
        except (OAuth2Client.DoesNotExist, OAuth2Code.DoesNotExist):
            return None

        if item.is_expired():
            return None

        return item

    def delete_authorization_code(self, auth_code):
        auth_code.delete_instance()

    def authenticate_user(self, auth_code):
        return auth_code.user


# OAUTH2 PASSWORD GRANT
class PasswordGrant(rfc6749.grants.ResourceOwnerPasswordCredentialsGrant):
    def authenticate_user(self, username, password):
        try:
            user = User.get(name=username)
        except User.DoesNotExist:
            return None
        # FIXME
        if user.check_password(password):
            return user


# OAUTH2 REFRESH-TOKEN GRANT
class RefreshTokenGrant(rfc6749.grants.RefreshTokenGrant):
    def authenticate_refresh_token(self, refresh_token):
        try:
            item = OAuth2Token.get(refresh_token=refresh_token)
        except OAuth2Token.DoesNotExist:
            return None

        if item.is_expired() or item.is_revoked():
            return None

        return item

    def authenticate_user(self, credential):
        return credential.user

    def revoke_old_credential(self, credential):
        credential.revoked = True
        credential.save()


# OAUTH2 EXTENSIONS
class RevocationEndpoint(rfc7009.RevocationEndpoint):
    def query_token(self, token, token_type_hint, client):
        try:
            if token_type_hint == 'access_token':
                return OAuth2Token.get(access_token=token)
            if token_type_hint == 'refresh_token':
                return OAuth2Token.get(refresh_token=token)

            tok = OAuth2Token.get(access_token=token)
            if not tok:
                tok = OAuth2Token.get(refresh_token=token)
            return tok
        except OAuth2Token.DoesNotExist:
            return None

    def revoke_token(self, token):
        token.revoked = True
        token.save()


# OIDC
class OidcMixin:
    def exists_nonce(self, nonce, request):
        try:
            client = OAuth2Client.get(id=request.client.id)
            code = OAuth2Code.get(client=client, nonce=nonce)
        except (OAuth2Client.DoesNotExist, OAuth2Code.DoesNotExist):
            return None
        return (code is not None)

    def get_jwt_config(self, grant):
        return app.config['OIDC']['JWT']

    @staticmethod
    def _generate_user_info(user, scope):
        # https://openid.net/specs/openid-connect-core-1_0.html#ScopeClaims
        user_info = UserInfo()

        if 'openid' in scope:
            user_info['sub'] = user.id
            pass
        if 'profile' in scope:
            user_info['name'] = user.name
            user_info['given_name'] = user.first_name
            user_info['family_name'] = user.last_name
        if 'email' in scope:
            user_info['email'] = user.email
        if 'address' in scope:
            # TODO: add address to User model
            # user_info['email'] = user.address
            pass
        if 'phone' in scope:
            # TODO: add phone number to User model
            # user_info['phone_number'] = user.phone_number
            pass

        return user_info

    def generate_user_info(self, user, scope):
        return self._generate_user_info(user, scope)


class OpenIDCode(OidcMixin, grants.OpenIDCode):
    pass


class OpenIDImplicitGrant(OidcMixin, grants.OpenIDImplicitGrant):
    pass


class OpenIDHybridGrant(OidcMixin, AuthCodeMixin, grants.OpenIDHybridGrant):
    pass


class BearerTokenValidator(rfc6750.BearerTokenValidator):
    def authenticate_token(self, token_string):
        try:
            return OAuth2Token.get(access_token=token_string)
        except OAuth2Token.DoesNotExist:
            return None


server = AuthorizationServer()
require_oauth = ResourceProtector()


def init(app):
    def query_client(client_id):
        try:
            return OAuth2Client.get(id=client_id)
        except OAuth2Client.DoesNotExist:
            return None

    def save_token(token_data, request):
        OAuth2Token.create(client=request.client, user=request.user,
                           issued_at=datetime.now(),
                           revoked=False,
                           **token_data)

    server.init_app(app, query_client=query_client,
                    save_token=save_token)

    require_oauth.register_token_validator(BearerTokenValidator())

    server.register_grant(AuthCodeGrant, [
        OpenIDCode(require_nonce=app.config['OIDC']['require_code_nonce']),
        rfc7636.CodeChallenge(
            required=app.config['OIDC']['require_code_challenge'])
    ])
    server.register_grant(rfc6749.grants.ImplicitGrant)
    server.register_grant(rfc6749.grants.ClientCredentialsGrant)
    server.register_grant(PasswordGrant)
    server.register_grant(RefreshTokenGrant)

    server.register_endpoint(RevocationEndpoint)

    server.register_grant(OpenIDImplicitGrant)
    server.register_grant(OpenIDHybridGrant)

    return server, require_oauth
