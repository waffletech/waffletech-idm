from flask import render_template, request, make_response, redirect, url_for, flash, abort
from flask_login import login_required, current_user, logout_user
from app import app, utils, models
import peewee

import bcrypt

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, SelectField
from wtforms.validators import DataRequired, Length


@app.route('/alert', methods=['GET'])
def alert():
    return make_response(render_template('alert.html'))


@app.route('/reset', methods=['GET'])
def reset():
    return make_response(render_template('reset.html'))


@app.route('/', methods=['GET'])
@login_required
def dashboard():
    if current_user.pw_reset:
        flash("You are required to set a new password.")
        return redirect(url_for('reset'))

    if current_user.ldap_disabled:
        # todo: move this to a decorator or something so it can apply to all views? same with above.
        flash("Your account is disabled. Please contact admin@waffle.tech for assistance.")
        logout_user()
        return redirect(url_for('login'))

    return make_response(render_template('dashboard.html'))


@app.route('/self/profile', methods=['GET', 'PUT'])
@login_required
def self_profile():
    # Read-only profile
    edit = request.args.get('edit', default=False)
    if request.method == 'GET' and not edit:
        return make_response(render_template('self/profile.html', edit=False))

    try:
        user = models.User.get(id=current_user.id)
    except peewee.DoesNotExist:
        return abort(404)

    class Form(FlaskForm):
        fname = StringField('First Name', validators=[
                            DataRequired(), Length(min=1, max=40)])
        lname = StringField('Last Name', validators=[
                            DataRequired(), Length(min=1, max=40)])
        email = StringField('Email', validators=[
                            DataRequired(), Length(min=1, max=40)])
        shell = SelectField('Login Shell', choices=[
                            '/usr/bin/bash', '/usr/bin/zsh'])

    form = Form()

    if request.method == 'PUT':
        if form.validate():
            user.first_name = form.fname.data
            user.last_name = form.lname.data
            user.email = form.email.data
            user.shell = form.shell.data
            user.save()
            flash("Profile saved.")
            utils.audit_log_other(user, 'profile-change',
                                  f"user changed profile: {form.fname.data}, {form.lname.data}, {form.email.data}, {form.shell.data}")
        else:
            flash("Error saving profile, please check your entries and try again.")

    form.fname.data = user.first_name
    form.lname.data = user.last_name
    form.email.data = user.email
    form.shell.data = user.shell

    return make_response(render_template('self/profile.html', form=form,
                                         edit=edit, current_user=user))


@app.route('/self/password', methods=['GET', 'PUT'])
@login_required
def self_password():
    edit = request.args.get('edit', default=False)
    if request.method == 'GET' and not edit:
        return make_response(render_template('self/password.html', edit=False))

    class Form(FlaskForm):
        cur_password = PasswordField('Current Password', validators=[DataRequired(), Length(min=1, max=100)])
        new_password = PasswordField('New Password', validators=[DataRequired(), Length(min=1, max=100)])
        confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), Length(min=1, max=100)])

    form = Form()

    if request.method == 'PUT':
        if form.validate():
            if not bcrypt.checkpw(form.cur_password.data.encode('utf-8'), current_user.ldap_password.encode('utf-8')):
                flash("Error changing password, please check your entries and try again.")
                form.cur_password.errors.append("Current password incorrect.")
            elif form.new_password.data != form.confirm_password.data:
                flash("Error changing password, please check your entries and try again.")
                form.new_password.errors.append("New password does not match confirm password.")
            else:
                current_user.ldap_password = bcrypt.hashpw(form.new_password.data.encode('utf-8'), bcrypt.gensalt())
                current_user.pw_reset = False
                current_user.save()
                flash("Password changed.")
                utils.audit_log_self('password-change', "user changed password")
        else:
            flash("Error changing password, please check your entries and try again.")

    return make_response(render_template('self/password.html', form=form, edit=edit))


@app.route('/self/audit', methods=['GET'])
@login_required
def self_audit():
    events = (models.AuditEvent.select()
              .where(models.AuditEvent.subject == current_user)
              .order_by(models.AuditEvent.timestamp))
    return make_response(render_template('self/audit.html',
                                         user=current_user, events=events))


@app.route('/self/apps', methods=['GET', 'POST'])
@login_required
def self_apps():
    pass
