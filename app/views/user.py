from flask import render_template, request, redirect, make_response, url_for, flash, abort
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, SelectField
from wtforms.validators import DataRequired, Length
from flask_login import login_user, login_required, logout_user, current_user
import peewee
from app import app, utils, login_manager, models
import random, string
import bcrypt


@app.route('/user', methods=['GET'])
@utils.admin_required
def user():
    users = models.User.select().where(models.User.system_user == False)
    if request.args.get('q'):
        users = users.where(models.User.name.contains(request.args.get('q')))

    return make_response(render_template('users.html', users=users))


class NewUserForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=1, max=30)])
    fname = StringField('First Name', validators=[DataRequired(), Length(min=1, max=30)])
    lname = StringField('Last Name', validators=[DataRequired(), Length(min=1, max=30)])
    email = StringField('Email', validators=[DataRequired(), Length(min=1, max=30)])
    submit_user = SubmitField('Create User')


@app.route('/user/new', methods=['GET', 'POST'])
@utils.admin_required
def user_new():
    form = NewUserForm()
    if form.validate_on_submit():
        if models.User.get_or_none(name=form.username.data):
            flash("An error occurred creating the new user.")
            form.username.errors.append("Username already in use.")
            return make_response(render_template('user/new.html', form=form))

        temp_password = ''.join(random.choices(string.ascii_letters + string.digits, k=12))
        new_user = models.User.make_user(form.fname.data, form.lname.data, form.username.data, form.email.data,
                                         temp_password)
        utils.audit_log_other(new_user, 'create-user', f"New user created: {new_user.name}")
        utils.audit_log_other(new_user, 'password-change', "Initial password set during user creation.")
        flash(f"New user created with temporary password: {temp_password}")
        return redirect(url_for('user_edit', user_id=new_user.id))

    return make_response(render_template('user/new.html', form=form))


class EditUserForm(FlaskForm):
    fname = StringField('First Name', validators=[DataRequired(), Length(min=1, max=40)])
    lname = StringField('Last Name', validators=[DataRequired(), Length(min=1, max=40)])
    email = StringField('Email', validators=[DataRequired(), Length(min=1, max=40)])
    shell = SelectField('Login Shell', choices=['/usr/bin/bash', '/usr/bin/zsh'])
    submit_profile = SubmitField('Save')


class UserAuthForm(FlaskForm):
    password = StringField('Password', validators=[DataRequired(), Length(min=1, max=100)])
    submit_password = SubmitField('Reset Password')


class UserDisableForm(FlaskForm):
    # todo: this should take a reason or an internal note for the audit log?
    submit_disable = SubmitField('Disable User')
    submit_enable = SubmitField('Enable User')


@app.route('/user/<int:user_id>', methods=['GET', 'POST'])
@utils.admin_required
def user_edit(user_id):
    form = EditUserForm()
    pwform = UserAuthForm()
    disform = UserDisableForm()
    try:
        user = models.User.get(id=user_id)
    except peewee.DoesNotExist:
        return abort(404)

    if form.submit_profile.data and form.validate():
        user.first_name = form.fname.data
        user.last_name = form.lname.data
        user.email = form.email.data
        user.shell = form.shell.data
        user.save()
        flash("Profile saved.")
        utils.audit_log_other(user, 'profile-change',
                              f"user changed profile: {form.fname.data}, {form.lname.data}, {form.email.data}, {form.shell.data}")
    elif form.submit_profile.data:
        flash("Error saving profile, please check your entries and try again.")
    else:
        form.fname.data = user.first_name
        form.lname.data = user.last_name
        form.email.data = user.email
        form.shell.data = user.shell

    if pwform.submit_password.data and pwform.validate():
        user.ldap_password = bcrypt.hashpw(pwform.password.data.encode('utf-8'), bcrypt.gensalt())
        user.pw_reset = True
        user.save()
        flash("Password reset.")
        utils.audit_log_other(user, 'password-change', "password reset by administrator. change required.")

    if disform.submit_disable.data and disform.validate():
        user.ldap_disabled = True
        user.save()
        flash("User disabled.")
        utils.audit_log_other(user, 'disable', "User disabled by administrator.")
    if disform.submit_enable.data and disform.validate():
        user.ldap_disabled = False
        user.save()
        flash("User enabled.")
        utils.audit_log_other(user, 'disable', "User re-enabled by administrator.")

    return make_response(render_template('user/edit.html', user=user, form=form, pwform=pwform, disform=disform))


class MembershipForm(FlaskForm):
    current_set = SelectField('Current', choices=[], validate_choice=False)
    candidate_set = SelectField('Options', choices=[], validate_choice=False)
    remove = SubmitField('Remove')
    add = SubmitField('Add')
    filter_query = StringField('Filter')
    filter = SubmitField('Search')


@app.route('/memberships/user/<int:user_id>', methods=['GET', 'POST'])
@utils.admin_required
def edit_memberships(user_id):
    try:
        user = models.User.get(id=user_id)
    except peewee.DoesNotExist:
        abort(404)
    form = MembershipForm()
    # Determine options a first time so form validation can check them
    current_set = user.groups
    candidate_set = [x for x in models.Group.select() if x not in current_set]
    if form.filter_query.data:
        candidate_set = [x for x in candidate_set if form.filter_query.data in x.name]
    form.current_set.choices = [x.name for x in current_set]
    form.candidate_set.choices = [x.name for x in candidate_set]

    if form.add.data and form.validate():
        if form.candidate_set.data:
            try:
                new_group = models.Group.get(name=form.candidate_set.data)
                user.add_group(new_group)
                utils.audit_log_other(user, "group", f"{user.name} added to {new_group.name}")
            except peewee.DoesNotExist:
                form.candidate_set.errors.append("Invalid choice.")
        else:
            form.candidate_set.errors.append("No choice selected.")

    if form.remove.data and form.validate():
        if form.current_set.data:
            try:
                del_group = models.Group.get(name=form.current_set.data)
                user.del_group(del_group)
                utils.audit_log_other(user, "group", f"{user.name} removed from {del_group.name}")
            except peewee.DoesNotExist:
                form.current_set.errors.append("Invalid choice.")
        else:
            form.current_set.errors.append("No choice selected.")

    # Determine options again in case they changed... refactoring could probably DRY this up
    current_set = user.groups
    candidate_set = [x for x in models.Group.select() if x not in current_set]
    if form.filter_query.data:
        candidate_set = [x for x in candidate_set if form.filter_query.data in x.name]
    form.current_set.choices = [x.name for x in current_set]
    form.candidate_set.choices = [x.name for x in candidate_set]

    return make_response(render_template('user/membership.html', form=form, user=user))
