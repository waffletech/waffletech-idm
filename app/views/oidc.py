from authlib.integrations.flask_oauth2 import current_token
from authlib.oauth2 import OAuth2Error

from flask import render_template, request, make_response, redirect, url_for, flash
from flask_login import login_required, current_user, logout_user

from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SelectField, TextAreaField
from wtforms.validators import DataRequired

from app import app, oidc
from app.oidc import server, require_oauth


@app.route('/oidc/authorize', methods=['GET', 'POST'])
@login_required
def oidc_authorize():
    if request.method == 'GET':
        try:
            grant = server.get_consent_grant(end_user=current_user)
        except OAuth2Error as err:
            return err.error

        client = grant.client
        scope = client.get_allowed_scope(grant.request.scope)

        if not client.consent_required:
            return server.create_authorization_response(grant_user=current_user)

        return render_template(
            'oidc_authorize.html',
            grant=grant,
            client=client,
            scope=scope
        )

    if request.form['submit'] == 'approve':
        return server.create_authorization_response(grant_user=current_user)

    return server.create_authorization_response(grant_user=None)


@app.route('/oidc/token', methods=['POST'])
def oidc_token():
    return server.create_token_response()


@app.route('/oidc/revoke', methods=['POST'])
def oidc_revoke():
    return server.create_endpoint_response(oidc.RevocationEndpoint.ENDPOINT_NAME)


@app.route('/oidc/userinfo', methods=['POST'])
@require_oauth('profile')
def oidc_userinfo():
    return oidc.OidcMixin._generate_user_info(current_token.user, current_token.scope)
