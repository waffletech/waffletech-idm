from flask import render_template, request, redirect, make_response, url_for, flash, abort
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, SelectField
from wtforms.validators import DataRequired, Length
from flask_login import login_user, login_required, logout_user, current_user
import peewee
from app import app, utils, login_manager, models
import random, string
import bcrypt


@app.route('/group', methods=['GET'])
@utils.admin_required
def group():
    groups = models.Group.select()
    if request.args.get('q'):
        groups = groups.where(
            models.Group.name.contains(request.args.get('q')))
    return make_response(render_template('groups.html', groups=groups))


class NewGroupForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired(), Length(min=1, max=30)])
    submit_group = SubmitField('Create Group')


@app.route('/group/new', methods=['GET', 'POST'])
@utils.admin_required
def group_new():
    form = NewGroupForm()
    if form.validate_on_submit():
        if models.Group.get_or_none(name=form.name.data):
            flash("An error occurred creating the new group.")
            form.name.errors.append("Name already in use.")
            return make_response(render_template('group/new.html', form=form))

        new_group = models.Group()
        new_group.name = form.name.data
        new_group.gid = models.IdSequence.next_id('gid')
        new_group.save()
        utils.audit_log_self('create-group', f"New user created: {new_group.name}")
        flash(f"New group created.")
        return redirect(url_for('edit_group', group_id=new_group.id))

    return make_response(render_template('group/new.html', form=form))


@app.route('/group/<int:group_id>', methods=['GET', 'POST'])
@utils.admin_required
def group_edit(group_id):
    try:
        group = models.Group.get(id=group_id)
    except peewee.DoesNotExist:
        return abort(404)

    return make_response(render_template('group/edit.html', group=group))
