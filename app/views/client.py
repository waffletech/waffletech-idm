from authlib.integrations.flask_oauth2 import current_token
from authlib.oauth2 import OAuth2Error

from flask import render_template, request, make_response, redirect, url_for, flash
from flask_login import login_required, current_user, logout_user

from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SelectField, TextAreaField
from wtforms.validators import DataRequired

from app import app, models


@app.route('/client', methods=['GET', 'POST'])
def client():
    class Form(FlaskForm):
        client_name = StringField(validators=[DataRequired()])
        client_uri = StringField(validators=[DataRequired()])

        scope_openid = BooleanField(default=True)
        scope_profile = BooleanField(default=True)
        scope_email = BooleanField(default=False)
        scope_address = BooleanField(default=False)
        scope_phone = BooleanField(default=False)

        redirect_uris = TextAreaField()

        grant_authorization_code = BooleanField(default=True)
        grant_implicit = BooleanField(default=True)
        grant_client_credentials = BooleanField(default=False)
        grant_password = BooleanField(default=False)
        grant_refresh_token = BooleanField(default=True)

        response_id_token = BooleanField(default=True)
        response_code = BooleanField(default=False)
        response_token = BooleanField(default=False)

        consent_required = BooleanField(default=True)
        auth_method = SelectField(choices=(
            'client_secret_basic',
            'client_secret_post',
            'client_secret_jwt',
            'private_key_jwt',
            'none',
        ))

    form = Form()

    if form.validate_on_submit():
        models.OAuth2Client.new(
            client_name=form.client_name.data,
            client_uri=form.client_uri.data,
            redirect_uris=form.redirect_uris.data.split('\r\n'),
            scopes=list((key for (key, val) in dict(
                openid=form.scope_openid.data,
                profile=form.scope_profile.data,
                email=form.scope_email.data,
                address=form.scope_address.data,
                phone=form.scope_phone.data
            ).items() if val)),
            grant_types=list((key for (key, val) in dict(
                authorization_code=form.grant_authorization_code.data,
                implicit=form.grant_implicit.data,
                password=form.grant_password.data,
                client_credentials=form.grant_client_credentials.data,
                refresh_token=form.grant_refresh_token.data
            ).items() if val)),
            response_types=list((key for (key, val) in dict(
                id_token=form.response_id_token.data,
                code=form.response_code.data,
                token=form.response_token.data
            ).items() if val)),
            consent_required=form.consent_required.data,
            auth_method=form.auth_method.data
        )


    return render_template('clients.html',
                           clients=models.OAuth2Client.select(),
                           form=form)
