from flask import render_template, request, redirect, make_response, url_for, flash
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, SelectField
from wtforms.validators import DataRequired, Length
from flask_login import login_user, login_required, logout_user, current_user
import peewee
import bcrypt
from app import app, utils, login_manager, models


@login_manager.user_loader
def load_user(user_id):
    return models.User.get_or_none(id=str(user_id))


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=1, max=30)])
    password = PasswordField('Password', validators=[DataRequired(), Length(min=1, max=100)])


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        try:
            user = models.User.get(name=form.username.data)
        except peewee.DoesNotExist:
            form.username.errors.append("No such user.")
            return render_template('auth/login.html', form=form)

        if not bcrypt.checkpw(form.password.data.encode('utf-8'), user.ldap_password.encode('utf-8')):
            form.password.errors.append("Incorrect password.")
            return render_template('auth/login.html', form=form)

        if user.ldap_disabled:
            flash(f"Your account is disabled. Please contact {app.config['SUPPORT_EMAIL']} for assistance.")
            form.username.errors.append("User disabled.")
            return render_template('auth/login.html', form=form)

        login_user(user)
        app.logger.debug(f"Logged in {user.name} ({user.id})")
        utils.audit_log_self('login', f"successful login from {request.remote_addr}")

        next_url = request.args.get('next')
        if not utils.is_safe_url(next_url):
            next_url = None

        if user.pw_reset:
            flash("You are required to set a new password.")
            next_url = url_for('reset')

        return redirect(next_url or url_for('dashboard'))

    return make_response(render_template('auth/login.html', form=form))


@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("You have been logged out.")
    return redirect(url_for('login'))
