from datetime import datetime, timedelta
import bcrypt
import secrets

from authlib.oauth2.rfc6749 import list_to_scope
from flask_login import AnonymousUserMixin
from playhouse.db_url import connect
from playhouse.fields import PickleField
from werkzeug.security import gen_salt
import peewee as pw

from app import app

database = connect(app.config["DATABASE"])


class CslField(pw.Field):
    field_type = 'varchar'

    def db_value(self, value):
        return ','.join(value)

    def python_value(self, value):
        return str(value).split(',')


class Group(pw.Model):
    id = pw.AutoField(column_name="id")
    name = pw.TextField(column_name="name")
    gid = pw.IntegerField(column_name="gidnumber")

    class Meta:
        database = database
        table_name = "groups"

    @property
    def members(self):
        r = []
        for user in User.select():
            if self in user.groups:
                r.append(user)
        return r


class User(pw.Model):
    id = pw.AutoField(column_name="id")
    name = pw.TextField(column_name="name")
    first_name = pw.TextField(column_name="givenname")
    last_name = pw.TextField(column_name="sn")
    email = pw.TextField(column_name="mail")
    shell = pw.TextField(column_name="loginshell")
    home = pw.TextField(column_name="homedirectory")
    ldap_disabled = pw.BooleanField(column_name="disabled", default=False)
    _pass_sha256 = pw.TextField(column_name="passha256", null=True)
    ldap_password = pw.TextField(column_name="passbcrypt")
    otp_secret = pw.TextField(column_name="otpsecret", null=True)
    yubikey = pw.TextField(column_name="yubikey", null=True)
    ssh_keys = pw.TextField(column_name="sshkeys", null=True)
    ldap_attributes = pw.TextField(column_name="custattr", null=True, default='{}')
    uid = pw.IntegerField(column_name="uidnumber")
    primary_group = pw.ForeignKeyField(Group, backref="descendents", field='gid', column_name="primarygroup")
    _other_groups = pw.TextField(column_name="othergroups", null=True)
    pw_reset = pw.BooleanField(column_name="pw_reset", default=True)
    system_user = pw.BooleanField(column_name="system_user", default=False)

    class Meta:
        database = database
        table_name = "users"

    # flask-login required stuff
    is_active = pw.BooleanField(default=True)

    @property
    def groups(self):
        groups = []
        if self._other_groups:
            gids = str(self._other_groups).split(",")
            for gid in gids:
                groups.append(Group.get(Group.gid == gid))
        return groups

    def del_group(self, group):
        if self._other_groups:
            group_ids = self._other_groups.split(",")
            if str(group.gid) in group_ids:
                group_ids.remove(str(group.gid))
                self._other_groups = ",".join(group_ids)
                self.save()

    def add_group(self, group):
        if self._other_groups:
            group_ids = str(self._other_groups).split(",")
            if str(group.gid) not in group_ids:
                group_ids.append(str(group.gid))
            self._other_groups = ",".join(group_ids)
            self.save()
        else:
            self._other_groups = str(group.gid)
            self.save()

    def get_audit_events(self, type):
        return AuditEvent.select().where(AuditEvent.subject == self,
                                         AuditEvent.type == type).order_by(AuditEvent.timestamp.desc())

    @property
    def is_admin(self):
        admin_group = Group.get(name='admin')
        if admin_group in self.groups:
            return True
        else:
            return False

    @property
    def is_authenticated(self):
        return self.is_active

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    @classmethod
    def make_user(cls, fname, lname, username, email, password):
        user_group = Group.get(name="users")

        n = cls()
        n.name = username
        n.first_name = fname
        n.last_name = lname
        n.email = email
        n.shell = "/usr/bin/bash"
        n.home = f"/home/{username}"
        n.ldap_disabled = False
        n.uid = IdSequence.next_id('uid')
        n.ldap_password = bcrypt.hashpw(
            password.encode('utf-8'), bcrypt.gensalt())
        n.pw_reset = True
        n.primary_group = user_group
        n.save()
        return n


class AnonymousUser(AnonymousUserMixin):
    def __init__(self):
        super(AnonymousUser, self).__init__()


class Capability(pw.Model):
    id = pw.AutoField()
    userid = pw.ForeignKeyField(User, backref="capabilities")
    action = pw.TextField()
    object = pw.TextField()

    class Meta:
        database = database
        table_name = "capabilities"


class IncludeGroup(pw.Model):
    id = pw.AutoField()
    parentgroupid = pw.IntegerField()
    includegroupid = pw.IntegerField()

    class Meta:
        database = database
        table_name = "includegroups"


class IdSequence(pw.Model):
    type = pw.TextField()
    counter = pw.IntegerField()

    class Meta:
        database = database
        table_name = "idtracker"

    @classmethod
    def next_id(cls, type):
        current_val = cls.get(cls.type == type)
        current_val.counter = current_val.counter + 1
        current_val.save()
        return current_val.counter


class AuditEvent(pw.Model):
    id = pw.AutoField()
    timestamp = pw.DateTimeField()
    actor = pw.ForeignKeyField(User)
    subject = pw.ForeignKeyField(User)
    type = pw.TextField()
    message = pw.TextField()

    class Meta:
        database = database
        table_name = "auditevent"


class OAuth2Client(pw.Model):
    id = pw.CharField(24, primary_key=True, unique=True)
    secret = pw.CharField(120)
    client_name = pw.CharField()
    client_uri = pw.CharField()
    redirect_uris = CslField()
    scopes = CslField()
    grant_types = CslField()
    response_types = CslField()
    consent_required = pw.BooleanField(default=True)
    auth_method = pw.CharField()

    issued_at = pw.DateTimeField()

    class Meta:
        database = database

    @classmethod
    def new(cls, auth_method='none', **kwargs):
        id_ = gen_salt(24)
        secret = gen_salt(48) if auth_method != 'none' else None
        return cls.create(id=id_,
                          secret=secret,
                          issued_at=datetime.now(),
                          auth_method=auth_method,
                          **kwargs)

    def get_client_id(self):
        return self.id

    def get_default_redirect_uri(self):
        return self.redirect_uris[0]

    def get_allowed_scope(self, scope):
        if not scope:
            return ''

        return list_to_scope(list(self.scopes))

    def check_redirect_uri(self, redirect_uri):
        return redirect_uri in self.redirect_uris

    def check_client_secret(self, client_secret):
        return secrets.compare_digest(str(self.secret), client_secret)

    def check_endpoint_auth_method(self, method, endpoint):
        if endpoint == 'token':
            return self.auth_method

        # TODO: not explained in authlib example, check RFC for guidance
        return True

    def check_response_type(self, response_type):
        return response_type in self.response_types

    def check_grant_type(self, grant_type):
        return grant_type in self.grant_types


class OAuth2Token(pw.Model):
    client = pw.ForeignKeyField(OAuth2Client)
    user = pw.ForeignKeyField(User)

    token_type = pw.CharField(40)
    scope = pw.TextField()

    access_token = pw.CharField(255, unique=True)
    refresh_token = pw.CharField(255, null=True)

    issued_at = pw.DateTimeField()
    expires_in = pw.IntegerField()
    revoked = pw.BooleanField(default=False)

    class Meta:
        database = database

    def check_client(self, client):
        return self.client.id == client.id()

    def get_scope(self):
        return self.scope

    def get_expires_in(self):
        return self.expires_in

    def is_revoked(self):
        return self.revoked

    def is_expired(self):
        offset = timedelta(seconds=self.expires_in)
        return self.issued_at + offset < datetime.now()


class OAuth2Code(pw.Model):
    code = pw.CharField(120, unique=True)

    client = pw.ForeignKeyField(OAuth2Client)
    user = pw.ForeignKeyField(User)

    redirect_uri = pw.TextField(null=True)
    response_type = pw.TextField()
    scope = pw.TextField()
    nonce = pw.TextField()

    code_challenge = pw.TextField(null=True)
    code_challenge_method = pw.CharField(40, null=True)

    issued_at = pw.DateTimeField()
    expires_in = pw.IntegerField()

    class Meta:
        database = database

    def is_expired(self):
        offset = timedelta(seconds=self.expires_in)
        return self.issued_at + offset < datetime.now()

    def get_redirect_uri(self):
        return self.redirect_uri

    def get_scope(self):
        return self.scope

    def get_auth_time(self):
        return self.issued_at.timestamp()

    def get_nonce(self):
        return self.nonce
