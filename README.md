# Waffle IDM

waffle idm is (or more accurately, will be) a very lightweight directory
solution for waffle.tech. It consists of a simple web application which
manages a database of users, and LDAP (via glauth) and oauth for applications
and Linux machines to authenticate against that database.

## Development Setup

1. Clone the repo (you could probably figure this out)
2. Install deps with `poetry install`
3. Create a directory `instance` in the root of the repo. Copy `examples/config.toml` to `instance/` and modify to
   taste.
4. Run `poetry run flask init-db` from the root of the repo to set up the initial database contents.
5. Run `poetry run flask run --debug` to start in development mode.
6. Browse to `localhost:5000` and you should be able to log in with the user you created during the init-db step.

## glauth

Waffle IDM is designed specifically so that glauth, a lightweight LDAP server, can use its database as a backend. A todo
is to make this easy, probably by putting example configs and systemd units here. For the time being, you can configure
glauth to use the Waffle IDM sqlite database as a backend.